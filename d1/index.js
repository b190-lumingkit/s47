// document refers to the whole webpage
/* document.getElementById("txt-first-name");
document.getElementById("txt-last-name");
document.getElementByTagName("input"); */

// querySelector replaces the three "getElement" selectors and makes use of css format in terms of selecting the elements inside the html as its arguments (# - id, . - class, tagName - tag)
document.querySelector("#txt-first-name")

// Event Listeners
// events are all interactions of the user to our webpage; such examples are clicking, hovering, reloading, keypress/keyup (typing)
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// to perform an action when an event triggers, first you have to listen to it
/* 
    .addEventListener = allows a block of code to listen to an event for them to be executed
        - takes two arguments: 1 - a string that identifies the event to which the codes will listen, 2 - a function that the listener will execute once the specified event is triggered
*/
txtFirstName.addEventListener("keyup", (e) => 
{
    // .innerHTML - allows the element to record/duplicate the value of the selected variable
    spanFullName.innerHTML = txtFirstName.value
})

document.addEventListener("keyup", (e) => 
{
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
})

txtFirstName.addEventListener("keyup", (e) => 
{
    // e - contains the information on the triggered event passed from the first argument
    // e.target - contains the element where the event happened
    // e.target.value - gets the value of the input object where the event happened
    console.log(e.target);
    console.log(e.target.value)
})
